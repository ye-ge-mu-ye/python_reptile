from multiprocessing import Process
import time


# def index():
#     print(f'子进程t1输出：8888')
#
# # if __name__ == '__main__':
# #     t1 = Process(target=index())
# #     t1.start()
# #     print('主进程输出')
#
# if __name__ == '__main__':
#     t1 = Process(target=index)
#     t1.start()
#     time.sleep(1)
#     print('主进程输出')

def index(num):
    print(f'子进程t1输出：8888')


if __name__ == '__main__':
    t1 = Process(target=index, args=(2,))
    # 子进程传参
    # args:元组形式传参
    t2 = Process(target=index, args=(1,))
    # kwargs:以字典形式传参
    t3 = Process(target=index, kwargs={'num': 3})
    t1.start()
    print('主进程输出')
    t2.start()
    t3.start()
