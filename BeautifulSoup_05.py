from bs4 import BeautifulSoup
html = """
<html>
    <head>
        <title>BS4与CSS选择器</title>
    </head>
    <body>
        <p class="title">处理html</p>
        <ul class="xiaoke1">
            <li class="xiaoke">可儿1</li>
            <li class="xiaoke">可儿2</li>
            <li class="xiaoke">可儿3</li>
            <li class="xiaoke">可儿4</li>
            <li class="xiaoke">可儿5</li>
        </ul>
        <a id="123456" name="第二个标签">2221</a>
        <a >2222</a>
        <a>2223</a>
        <a>2224</a>
        <a>2225</a>
    </body>
"""
soup = BeautifulSoup(html,'lxml')
# print(soup.prettify())
# title = soup.html.head.title.string
# print(title)

# print(soup.body.name)
# print(soup.body)
# print(soup.p.attrs)
# print(soup.p.attrs['class'])
# print(soup.a.attrs['id'])
# print(soup.a.attrs['name'])

# 获取子节点
# print(soup.ul.descendants)
# for i in soup.ul.descendants:
#     print(i)

# 获取父节点
# print(soup.li.parent)

# 获取所有同级标签
# for i in soup.li.next_siblings:
#     print(i)

# print(soup.find_all('li'))
# for i in soup.find_all('li'):
#     print(i.string)
#     # for j in i:
#     #     print(j.string)


# 根据属性值筛选过滤
for ul in soup.find_all(attrs={'class':'xiaoke'}):
    print(ul,'--------')


for li in soup.find_all(class_='xiaoke'):
    print(li,'+++++++')


print(soup.find_all('li',{'class','xiaoke'}))

# css选择器方 ：soup.select(css选择器)，例如print(soup.select(div>ui>li))

