from threading import Thread

# def index(num):
#     print(f'我是{num}')
#
#
# if __name__ == '__main__':
#     t1 = Thread(target=index,args=('t1',))
#
#     t2 = Thread(target=index, kwargs={'num':'t2'})
#
#     t1.start()
#     t2.start()

# 线程队列
from threading import Thread
from queue import Queue
import time
class Spider():
    def __init__(self):
        self.q = Queue()

    def parse_put_job(self):
        for day in range(1,51):
            data = f"第{day}天，我依然爱你"
            self.q.put(data)
            self.q.join()


    def parse_get_job(self):
        while True:
        # for i in range(1,51):
            data = self.q.get()
            print(data,'--------获取任务--------')
            self.q.task_done()


    def run(self):
        t1 = Thread(target=self.parse_put_job)
        t2 = Thread(target=self.parse_get_job)
        t1.start()
        t2.daemon = True
        t2.start()
        t1.join()

if __name__ == '__main__':
    s = Spider()
    s.run()
    print('主线程执行结束')



